from django.urls import path
from projects.views import projects, details, create_project


urlpatterns = [
    path("", projects, name="list_projects"),
    path("<int:id>/", details, name="show_project"),
    path("create/", create_project, name="create_project"),
]
